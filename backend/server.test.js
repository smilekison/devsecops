const http = require('https');

describe('Test the test environment', () => {
    test('Check if Jest works', () => {
        expect(1).toBe(1);
    });
});

describe('Test the root path', () => {
    test('It should response the GET method', done => {
        http.get('https://product.smilekisan.com/', res => {
            expect(res.statusCode).toBe(200);
            done();
        });
    },10000);
});

describe('Test the /api/users path', () => {
    test('It should response the GET method', done => {
        http.get('https://product.smilekisan.com/api/users', res => {
            expect(res.statusCode).toBe(404);
            done();
        });
    }, 25000);
});